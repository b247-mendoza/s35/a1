const express = require("express");
const mongoose = require("mongoose");

const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect("mongodb+srv://mainemendoza:mavis@zuitt-bootcamp.waf5lts.mongodb.net/s35-activity?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("We're connected to the cloud database"));

const userSchema = new mongoose.Schema({
	username: String,
	password: String
});

const User = mongoose.model("User", userSchema);
app.post("/signup", (request, response) => {
	User.findOne({username: request.body.username},(error, result) => {
		if(result != null && result.username == request.body.username){
			return response.send("Duplicate username found!")
		}
		let newUser = new User({
			username: request.body.username,
			password: request.body.password
		})
		newUser.save((error, saveUser) => {
			if(error){
				return console.error(error)
			} else {
				return response.status(200).send('New user signed up')
			}
		})
	})
})

app.get("/signup", (req, res) => {
	User.find({},(err, result) =>{
		if(err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				data : result
			})
		}
	})
})

app.listen(port, () => console.log(`Server is now running at port ${port}`));